#!/bin/bash

################################################################################
# install.sh                                                                   #
# Installaton script of dotfiles                                               #
#                                                                              #
# Author: Simi (simon.pavel@gmail.com)                                         #
# Hosted on:  https://gitlab.com/Simi/dotfiles/tree/master                     #
################################################################################

#Global variables
PACKAGES=()
DOTFILES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BACKUP_DIR="$DOTFILES_DIR/.backup"

function usage {
  echo "Usage: `basename $0` [OPTION] [packgage list]"
  echo 
  echo "-h, --help	Print this message"
  echo "-a, --all	Install all packages. This overides any package entered on cmd line."
  echo 
  echo "package list is separated by spaces"
  echo "example: ./install.sh tmux mplayer"
  exit 0
}

#search all packages, this overwrite any given package in command line
function searchAll {
  PACKAGES=()
  for dir in `ls -d $DOTFILES_DIR/*/`
  do
    dir=(${dir%%/})
    PACKAGES+=(${dir##*/})
  done
}

#parse parameters
all=false
while [[ $# > 0 ]]
do
  key="$1"
  shift

  case $key in
    -h|--help)
      usage
      ;;
    -a|--all)
      searchAll
      all=true
      ;;
    *)
      if ! $all; then
        PACKAGES+=($key);
      fi
      ;;
    esac
done

#if we dont have any parameter show usage and exit
if [ ${#PACKAGES[@]} -eq 0 ]; then
  usage
fi

#create backup dir if not already exists
mkdir $BACKUP_DIR 2> /dev/null

#process packages to install
for pack in "${PACKAGES[@]}"
do
  #verify if package exist and we have install file
  if [ ! -d "$DOTFILES_DIR/$pack" ]; then
    echo "Error package '$pack' doesn't exist. SKIPPING" >&2
    continue
  fi
  if [ ! -f "$DOTFILES_DIR/$pack/install" ]; then
    echo "Error package '$pack' is missing installation file. SKIPPING" >&2
    continue
  fi

  #we can proceed to package installation
  echo "Installation of package '$pack' started"

  #iterate in install file
  while read line; #$DOTFILES_DIR/$pack/install
  do  
    file=$DOTFILES_DIR/$pack/${line% *}
    location=${line##* }
    location=${location/#\~/$HOME}
    #check if file for installation exists
    if [ ! -f "$file" ]; then
      echo "Error file $file doesn't exist. SKIPPING" >&2
      continue
    fi
    
    #check if file is not installed already
    if [[ -L $location && -f $location ]]; then
      echo "Notice file $file already installed. SKIPPING" >&2
      continue
    fi

    #check if we need backup original file
    if [ -f "$location" ]; then
      echo "Backuping original file $location to $BACKUP_DIR"
      mkdir $BACKUP_DIR/$pack 2> /dev/null
      if [ -f $BACKUP_DIR/$pack/${location##*/} ]; then
        echo "Error backup file already exist!!! Resolve this manualy. SKIPPING"
        continue
      fi
      mv $location $BACKUP_DIR/$pack
    fi

    #install file using symlink 
    echo "Installing file $location"
    install_dir=${location%/*}
    mkdir $install_dir 2> /dev/null   #create folder if needed
    ln -s $file $location
  done < $DOTFILES_DIR/$pack/install
done
